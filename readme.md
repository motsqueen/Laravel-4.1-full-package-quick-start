# 已将项目迁移至 [github.com/5-say/laravel-4.1-quick-start-cn](https://github.com/5-say/laravel-4.1-quick-start-cn) ，此处不再更新。




## laravel-4.1-quick-start

Laravel 4.1.* 完整包（包含vendor目录和一个小demo，扩展了数据库迁移的 MySQL 注释支持），方便 composer 缓慢的朋友快速开始开发，常用源码已补充中文注释。

作为 [cnComment-Laravel-4](http://git.oschina.net/chengwu/cnComment-Laravel-4) 项目的延伸，原项目将停止源码方面的更新，仅保留做学习资料的搜集与推荐。

## 关于 本项目加入的辅助元素（与原版的区别）

- 请点击 [different.md](mdDoc/different.md) 查看。
- **新增 migrate 数据库迁移的 MySQL 注释支持** 详细内容请参阅这篇博文：[让 Laravel 4.1 的“数据库迁移”支持 MySQL 字段注释](http://my.oschina.net/5say/blog/186017)

## 关于 Demo

- 短链生成[点击查看说明](mdDoc/demoUrlShortened.md)
 - 源于 **袁维隆** 的 *[Laravel4快速上手教程](http://beta.zexeo.com/course/3) 19-22 讲* 稍做改动，建议配合视频学习。